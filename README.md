# Parks

Parks is an app to track which local parks you and your family have visited.

Each time you visit, you can comment about your favorite feature and collect a feature to use when building your own virtual park.
