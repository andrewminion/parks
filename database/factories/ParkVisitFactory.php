<?php

namespace Database\Factories;

use App\Models\Park;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ParkVisit>
 */
class ParkVisitFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'park_id' => Park::factory(),
            'user_id' => User::factory(),
            'notes' => fake()->paragraph(),
        ];
    }
}
