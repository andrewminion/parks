<?php

use App\Models\Park;
use App\Models\ParkVisit;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('park_visits', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Park::class)->constrained();
            $table->foreignIdFor(User::class)->constrained();
            $table->longText('notes')->nullable();
            $table->timestamps();
        });

        Schema::create('park_visit_user', function (Blueprint $table) {
            $table->foreignIdFor(ParkVisit::class)->constrained();
            $table->foreignIdFor(User::class)->constrained();
            $table->foreignIdFor(User::class, 'added_by_user_id')->constrained('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('park_visit_user');
        Schema::dropIfExists('park_visits');
    }
};
