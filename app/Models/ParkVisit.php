<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\ParkVisit
 *
 * @property int $id
 * @property int $park_id
 * @property int $user_id
 * @property string|null $notes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $otherUsers
 * @property-read int|null $other_users_count
 * @property-read \App\Models\Park $park
 * @property-read \App\Models\User $user
 *
 * @method static \Database\Factories\ParkVisitFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|ParkVisit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ParkVisit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ParkVisit query()
 * @method static \Illuminate\Database\Eloquent\Builder|ParkVisit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParkVisit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParkVisit whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParkVisit whereParkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParkVisit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParkVisit whereUserId($value)
 *
 * @mixin \Eloquent
 */
class ParkVisit extends Model
{
    use HasFactory;

    public function park(): BelongsTo
    {
        return $this->belongsTo(Park::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function otherUsers(): BelongsToMany
    {
        return $this
            ->belongsToMany(User::class)
            ->withPivot('added_by_user_id');
    }
}
