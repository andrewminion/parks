<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use MatanYadaev\EloquentSpatial\Objects\Geometry;

/**
 * App\Models\Park
 *
 * @property int $id
 * @property string $name
 * @property \MatanYadaev\EloquentSpatial\Objects\Geometry|null $location
 * @property string|null $address_1
 * @property string|null $address_2
 * @property string|null $city
 * @property string|null $state
 * @property string|null $zip
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\ParkVisit> $parkVisits
 * @property-read int|null $park_visits_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $visitors
 * @property-read int|null $visitors_count
 *
 * @method static \Database\Factories\ParkFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Park newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Park newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Park query()
 * @method static \Illuminate\Database\Eloquent\Builder|Park whereAddress1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Park whereAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Park whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Park whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Park whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Park whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Park whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Park whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Park whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Park whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Park whereZip($value)
 *
 * @mixin \Eloquent
 */
class Park extends Model
{
    use HasFactory;

    protected $casts = [
        'location' => Geometry::class,
    ];

    public function parkVisits(): HasMany
    {
        return $this->hasMany(ParkVisit::class);
    }

    public function visitors(): HasManyThrough
    {
        return $this->hasManyThrough(User::class, ParkVisit::class);
    }
}
